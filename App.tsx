import React from "react";
import Toast from "react-native-toast-message";

import HomeScreen from "./app/pages/HomeScreen";
import LocalStorage from "./app/pages/LocalStorage";
import { Providers } from "./app/components/providers/providers";
import { NativeRouter, Route, Routes } from "react-router-native";
import { View } from "react-native";
import Connect from "./app/pages/auth/Connect";
import Register from "./app/pages/auth/Register";
import ProductDesc from "./app/pages/Catalog/ProductDesc";
import BasketPage from "./app/pages/Basket/BasketPage";
import Profile from "./app/pages/Profile";
import Validate from "./app/pages/Command/Validate";
import CommandDetailPage from "./app/pages/Command/CommandDetailPage";
import CommandsPage from "./app/pages/Command/CommandsPage";

export default function App() {
  return (
    <Providers>
      <NativeRouter>
        <View>
          <Routes>
            <Route path="/" Component={HomeScreen} />
            <Route path="/basket" Component={BasketPage} />
            <Route path="/connect" Component={Connect} />
            <Route path="/register" Component={Register} />
            <Route path="/product/:idProduct" Component={ProductDesc} />
            <Route path="/profile" Component={Profile} />
            <Route path="/validate" Component={Validate} />
            <Route path="/command/:idCommand" Component={CommandDetailPage} />
            <Route path="/commands" Component={CommandsPage} />
          </Routes>
        </View>
      </NativeRouter>
      <Toast />
    </Providers>
  );
}
