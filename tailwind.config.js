/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./app/**/*.{js,jsx,ts,tsx}", "App.tsx"],
  theme: {
    extend: {
      colors: {
        primary: "#E8B10D",
        secondary: "#F2F2F2",
        gray: "#E9E9E9",
        lightBlack: "#222222",
      },
      boxShadow: {
        boxShadowTop: "0px -2px 2px rgba(0, 0, 0, 0.25)",
        boxShadowBottom: "0px 2px 2px rgba(0, 0, 0, 0.25)",
        button: "0px 3px 8px rgba(0, 0, 0, 0.24)",
      },
      height: {
        mobile: "100dvh",
      },
      keyframes: {
        "accordion-down": {
          from: { height: 0 },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: 0 },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
    },
  },
  plugins: [],
};
