export const subString = (str: string, length: number) => {
  if (str.length > length) {
    return str.substring(0, length) + "...";
  }
  return str;
};

export const formatDate = (date: Date) => {
  // Récupérer les différentes parties de la date
  const day = String(date.getDate()).padStart(2, "0");
  const month = String(date.getMonth() + 1).padStart(2, "0"); // Les mois commencent à 0
  const year = date.getFullYear();

  // Récupérer les différentes parties de l'heure
  const hours = String(date.getHours()).padStart(2, "0");
  const minutes = String(date.getMinutes()).padStart(2, "0");

  // Construire la chaîne de caractères au format JJ/MM/AAAA HH:mm
  return `${day}/${month}/${year} ${hours}:${minutes}`;
};
