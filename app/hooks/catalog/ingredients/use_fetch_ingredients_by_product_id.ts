import { useQuery } from "@tanstack/react-query";
import { apiFetch } from "../../../utils/fetch";
import { fetchProductIngredients } from "../../../types/types";

const fetchIngredientsIdsByProductID = async (id: number) => {
  return await apiFetch<fetchProductIngredients>(
    `/catalog/product-ingredients?id_product=${id}`
  );
};

export const useFetchIngredientsIdsByProductID = (id: number) => {
  return useQuery({
    queryKey: ["product-ingredients-by-id", id],
    queryFn: () => fetchIngredientsIdsByProductID(id),
  });
};
