import { useQuery } from "@tanstack/react-query";
import { Ingredient } from "../../../types/types";
import { apiFetch } from "../../../utils/fetch";

const fetchIngredientById = async (id: number) => {
  return await apiFetch<Ingredient>(`/catalog/ingredients/${id}`);
};

export const useFetchIngredientById = (id: number) => {
  return useQuery({
    queryKey: ["ingredient-by-id", id],
    queryFn: () => fetchIngredientById(id),
  });
};
