import { useQuery } from "@tanstack/react-query";
import { Product } from "../../../types/types";
import { apiFetch } from "../../../utils/fetch";

const fetchProductByID = async (id: number) => {
  return await apiFetch<Product>(`/catalog/products/${id}`);
};

export const useFetchProductByID = (id: number) => {
  return useQuery({
    queryKey: ["product-by-id", id],
    queryFn: () => fetchProductByID(id),
  });
};
