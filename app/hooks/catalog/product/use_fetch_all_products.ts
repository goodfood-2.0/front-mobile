import { useQuery } from "@tanstack/react-query";
import { apiFetch } from "../../../utils/fetch";
import { Product, fetchProduct } from "../../../types/types";

const fetchAllProducts = async () => {
  return await apiFetch<fetchProduct>(`/catalog/products`);
};

export const useFetchAllProducts = () => {
  return useQuery({
    queryKey: ["products-by-id"],
    queryFn: () => fetchAllProducts(),
  });
};
