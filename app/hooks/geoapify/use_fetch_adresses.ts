import { useMutation, useQuery } from "@tanstack/react-query";
import { apiFetch } from "../../utils/fetch";

const fetchAddresses = async (address: string) => {
  const response = await fetch(
    `https://api.geoapify.com/v1/geocode/autocomplete?text=${address}&format=json&apiKey=d500fac743b541c6af3f94326ab88921`,
    {
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
    }
  );

  if (response.ok) {
    return response.json() as Promise<any>;
  }

  throw new ApiError(response.status, await response.json());
};

export const useFetchAddresses = () => {
  return useMutation({
    mutationFn: (address: string) => {
      return fetchAddresses(address);
    },
  });
};

class ApiError extends Error {
  constructor(public status: number, public data: Record<string, unknown>) {
    if (status === 401) {
      localStorage.removeItem("account");
    }

    super(`ApiError: ${status} ${JSON.stringify(data)}`);
  }
}
