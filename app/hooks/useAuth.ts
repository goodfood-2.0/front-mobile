import { useCallback } from "react";
import { useUserStore } from "../store/useUserStore";
import { NavigateFunction } from "react-router-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { apiFetch } from "../utils/fetch";
import Toast from "react-native-toast-message";

export enum AuthStatus {
  Unknown = 0,
  Authenticated = 1,
  Guest = 2,
}

const enum Roles {
  Client = 1,
  Accountant = 2,
  Admin = 3,
}

export function useAuth() {
  const { user, setUser } = useUserStore();
  //const router = useRouter();
  //const { toast } = useToast();
  let status = AuthStatus.Unknown;
  let role = Roles.Client;

  switch (user) {
    case null:
      status = AuthStatus.Guest;
      break;
    case undefined:
      status = AuthStatus.Unknown;
      break;
    default:
      status = AuthStatus.Authenticated;
      break;
  }

  if (user && user.role) {
    switch (user && user.role.name) {
      case "accountant":
        role = Roles.Accountant;
        break;
      case "admin":
        role = Roles.Admin;
        break;
      default:
        role = Roles.Client;
        break;
    }
  }

  const authenticate = useCallback(() => {
    apiFetch("/auth/security/me")
      .then((value: any) => {
        setUser(value.user);
      })
      .catch(() => setUser(null));
  }, [setUser]);

  const login = useCallback(
    (email: string, password: string, navigate: NavigateFunction) => {
      apiFetch("/auth/security/login", {
        json: { email, password },
        method: "POST",
      })
        .then((value: any) => {
          AsyncStorage.setItem("accessToken", value.accessToken);
          AsyncStorage.setItem("refreshToken", value.refreshToken);

          navigate("/");

          /*setTimeout(() => {
            if (value.user._role.name === "accountant") {
              navigate("/partners");
            } else if (value.user._role.name === "admin") {
              navigate("/partners");
            } else {
              navigate("/");
            }
          }, 2000);*/
        })
        .then(authenticate)
        .catch(() => {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: "Informations de connexion erronées",
          });
        });
    },
    [authenticate]
  );

  const register = useCallback(
    (firstname: string, email: string, navigate: NavigateFunction) => {
      apiFetch("/auth/security/register", {
        json: { email: email, firstname: firstname, lastname: firstname },
      })
        .then(() => {
          Toast.show({
            type: "success",
            text1: "Réussite",
            text2:
              "Vous avez reçu un mail pour confirmer la création de compte 👋",
          });
          navigate("/connect");
        })
        .catch(() => {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: "Une erreur est survenue lors de la création de compte",
          });
        });
    },
    []
  );

  const logout = useCallback(
    (navigate: NavigateFunction) => {
      apiFetch("/auth/security/logout", { method: "DELETE" })
        .then(() => {
          AsyncStorage.removeItem("accessToken");
          AsyncStorage.removeItem("refreshToken");
        })
        .then(() => {
          Toast.show({
            type: "success",
            text1: "Déconnexion",
            text2: "Vous avez été déconnecté",
          });
        })
        .then(() => setUser(null))
        .then(() => {
          navigate("/");
        });
    },
    [setUser]
  );

  return {
    user,
    status,
    role,
    authenticate,
    login,
    logout,
    register,
  };
}
