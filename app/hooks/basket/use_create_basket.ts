import { useMutation } from "@tanstack/react-query";
import { apiFetch } from "../../utils/fetch";
import { Basket } from "../../types/types";

const createBasket = async (basket: { userId: string; products: any }) => {
  return await apiFetch(`/order/baskets`, {
    method: "POST",
    json: basket,
  });
};

export const useCreateBasket = () => {
  return useMutation({
    mutationFn: (payload: { userId: string; products: any }) => {
      return createBasket(payload);
    },
  });
};
