import { useMutation } from "@tanstack/react-query";
import { apiFetch } from "../../utils/fetch";

const updateBasket = async (basket: { userId: string; products: any }) => {
  return await apiFetch(`/order/baskets`, {
    method: "POST",
    json: { userId: basket.userId, products: basket.products },
  });
};

export const useUpdateBasket = () => {
  return useMutation({
    mutationFn: (payload: { userId: string; products: any }) => {
      return updateBasket(payload);
    },
  });
};
