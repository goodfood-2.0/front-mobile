import { useMutation } from "@tanstack/react-query";
import { apiFetch } from "../../utils/fetch";
import { Basket, OrderBody } from "../../types/types";

const validateBasket = async (order: {
  userId: string;
  orderInfo: OrderBody;
}) => {
  return await apiFetch(`/order/baskets/validate/${order.userId}`, {
    method: "POST",
    json: {
      email: order.orderInfo.email,
      idRestaurant: order.orderInfo.idRestaurant,
      country: order.orderInfo.country,
      city: order.orderInfo.city,
      address: order.orderInfo.address,
      additionnalAddress: order.orderInfo.additionnalAddress,
      zipCode: order.orderInfo.zipCode,
      commandType: order.orderInfo.commandType,
      createdAt: order.orderInfo.createdAt,
      updatedAt: order.orderInfo.updatedAt,
    },
  });
};

export const useValidateBasket = () => {
  return useMutation({
    mutationFn: (payload: { userId: string; orderInfo: OrderBody }) => {
      return validateBasket(payload);
    },
  });
};
