import { useQuery } from "@tanstack/react-query";
import { apiFetch } from "../../utils/fetch";
import { Order } from "../../types/types";

const fetchOrdersByEmail = async (email: string) => {
  return await apiFetch<Order[]>(`/order/orders?email=${email}`);
};

export const useFetchOrdersByEmail = (email: string) => {
  return useQuery({
    queryKey: ["orders-by-email", email],
    queryFn: () => fetchOrdersByEmail(email),
  });
};
