import { useQuery } from "@tanstack/react-query";
import { apiFetch } from "../../utils/fetch";
import { Order } from "../../types/types";

const fetchOrderById = async (id: number) => {
  return await apiFetch<Order>(`/order/orders/${id}`);
};

export const useFetchOrderById = (id: number) => {
  return useQuery({
    queryKey: ["order-by-id", id],
    queryFn: () => fetchOrderById(id),
  });
};
