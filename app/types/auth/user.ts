export type User = {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  has_notifications: boolean;
  created_at: Date;
  updated_at: Date;
  role: {
    _id: string;
    name: string;
    created_at: string;
    updated_at: string;
    __v: number;
  };
  restaurant: number;
  _restaurant: number;
};
