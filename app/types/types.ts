export type fetchProduct = {
  count: number;
  data: Product[];
};

export type Product = {
  ID: number;
  id: number;
  description: string;
  name: string;
  image: string;
  price: number;
  activated: boolean;
  id_restaurant: number;
  ingredients: any;
};

export type fetchProductIngredients = {
  data: ProductIngredients[];
};

export type ProductIngredients = {
  id_product: number;
  id_ingredient: number;
  required_quantity: number;
};

export type fetchIngredient = {
  data: Ingredient;
};

export type Ingredient = {
  ID: number;
  is_allergen: boolean;
  is_allergen_description: string;
  name: string;
  CreatedAt: string;
  DeletedAt: string;
  UpdatedAt: string;
};

export type Basket = {
  userId: string;
  products: Array<{
    idContent: number;
    contentName: string;
    quantity: number;
  }>;
};

export type ProductBasket = {
  idContent: number;
  contentName: string;
  quantity: number;
};

export type OrderContent = {
  idContent: number;
  contentName: string;
  quantity: number;
  price: number;
};

export type Order = {
  id?: number;
  email: string;
  idRestaurant: number;
  country: string;
  city: string;
  address: string;
  additionnalAddress: string;
  zipCode: string;
  commandType: string;
  isValidate: boolean;
  createdAt: string;
  updatedAt: string;
  orderContents: OrderContent[];
};

export type OrderBody = {
  email: string;
  idRestaurant: number;
  country: string;
  city: string;
  address: string;
  additionnalAddress: string;
  zipCode: string;
  commandType: string;
  createdAt: string;
  updatedAt: string;
};

export type Restaurant = {
  ID?: number;
  name: string;
  address: string;
  additional_address: string;
  city: string;
  zip_code: string;
  country: string;
};

export type Deliveries = {
  id: number;
  statusId: number;
  status: {
    id: number;
    myStatus: string;
  };
  id_Restaurant: number;
  id_Order: number;
  email: string;
  picturePath: string;
};
