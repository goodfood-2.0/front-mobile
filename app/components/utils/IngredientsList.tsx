import { styled } from "nativewind";
import { ProductIngredients } from "../../types/types";
import React from "react";
import { View, Text } from "react-native";
import { useFetchIngredientById } from "../../hooks/catalog/ingredients/use_fetch_ingredient_by_id";

type IngredientsListProps = {
  idIngredient: number;
};

const StyledText = styled(Text);

export const IngredientsList = ({ idIngredient }: IngredientsListProps) => {
  const ingredient = useFetchIngredientById(idIngredient);

  return <StyledText>- {ingredient.data?.name}</StyledText>;
};
