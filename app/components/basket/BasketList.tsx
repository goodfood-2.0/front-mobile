import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import { ProductBasket } from "../../types/types";
import { useFetchProductByID } from "../../hooks/catalog/product/use_fetch_product_by_id";
import Loader from "../Loader";
import { subString } from "../../utils/functions";
import { useNavigate } from "react-router-native";
import { MaterialIcons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useFetchRestaurantById } from "../../hooks/restaurants/use_fetch_restaurant_by_id";

type BasketListProps = {
  productBasket: ProductBasket;
  deleteFromBasket: (id: number) => void;
};

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTouchable = styled(TouchableOpacity);

const BasketList = ({ productBasket, deleteFromBasket }: BasketListProps) => {
  const navigate = useNavigate();
  const product = useFetchProductByID(productBasket.idContent);
  const restaurant = useFetchRestaurantById(
    product.data ? product.data?.id_restaurant : -1
  );

  if (product.isLoading || !product.data || restaurant.isLoading) {
    return <Loader />;
  }
  return (
    <>
      <StyledView className="w-full py-6 flex flex-row items-center relative">
        <StyledTouchable
          className="mr-2"
          onPress={() => navigate(`/product/${product.data.ID}`)}
        >
          <Image
            source={{
              uri: product.data.image,
            }}
            width={65}
            height={65}
            style={{
              borderRadius: 9999,
            }}
          />
        </StyledTouchable>
        <StyledView className="flex flex-col w-2/3 mr-3">
          <StyledView>
            <StyledText className="text-lg font-bold">
              {product.data.name}
            </StyledText>
          </StyledView>
          <StyledView className="w-9/12">
            <StyledText className="text-xs">
              {restaurant.data &&
                `${restaurant.data.name} - ${restaurant.data.address}, ${restaurant.data.zip_code} ${restaurant.data.city}`}
            </StyledText>
          </StyledView>
          <StyledView>
            <StyledText className="text-base text-primary font-semibold">
              {(product.data.price * productBasket.quantity)
                .toFixed(2)
                .padEnd(2, "0")}
              €
            </StyledText>
          </StyledView>
        </StyledView>
        <StyledView className="">
          <StyledText>{productBasket.quantity}</StyledText>
        </StyledView>
        <StyledTouchable
          className="absolute right-5 bottom-2"
          onPress={() => deleteFromBasket(product.data.ID)}
        >
          <MaterialIcons name="delete-outline" size={24} color="#222222" />
        </StyledTouchable>
      </StyledView>
      <StyledView className="w-full flex justify-center items-center">
        <StyledView className="h-px w-5/6 bg-zinc-300" />
      </StyledView>
    </>
  );
};

export default BasketList;
