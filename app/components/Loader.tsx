import { AntDesign } from "@expo/vector-icons";
import { styled } from "nativewind";
import { View, Text, ScrollView } from "react-native";
import React from "react";
import Header from "../pages/Header";
import Navbar from "../pages/Navbar";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);

const Loader = () => {
  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView className="">
        <StyledView className="flex h-[550px] justify-center items-center">
          <StyledText className="text-2xl tracking-widest">
            Loading...
          </StyledText>
        </StyledView>
      </StyledScrollView>
      <Navbar />
    </StyledView>
  );
};

export default Loader;
