import { styled } from "nativewind";
import React, { useState } from "react";
import { Basket, Product } from "../../types/types";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { useNavigate } from "react-router-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";
import { useAuth } from "../../hooks/useAuth";
import { useCreateBasket } from "../../hooks/basket/use_create_basket";
import { useUpdateBasket } from "../../hooks/basket/use_fetch_update_basket";
import { useFetchBasketByUserID } from "../../hooks/basket/use_fetch_basket_by_id";
import { QueryClient } from "@tanstack/react-query";

type ProductCardProps = {
  product: Product;
  resetBasketFromChild: (n: boolean) => void;
};

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTouchable = styled(TouchableOpacity);

const ProductCard = ({ product, resetBasketFromChild }: ProductCardProps) => {
  const navigate = useNavigate();

  const { user } = useAuth();

  const addToBasket = useCreateBasket();
  const updateBasket = useUpdateBasket();
  const userBasket = useFetchBasketByUserID(user ? user._id : "");
  const [resetBasket, setResetBasket] = useState<boolean>(false);

  const queryClient = new QueryClient();

  const onPressCreateBasket = async (product: Product) => {
    //AJOUTER LA QUANTITE QUAND ON AURA LE DROPDOWN

    const contentObject = {
      idContent: product.id,
      contentName: product.name,
      quantity: 1,
    };

    //Si l'utilisateur est connecté
    if (user) {
      //Si le panier n'est pas vide
      if (userBasket.data) {
        for (let i = 0; i < userBasket.data.products.length; i++) {
          if (userBasket.data.products[i].idContent == product.id) {
            userBasket.data.products[i].quantity += 1;
            updateBasket
              .mutateAsync({
                userId: user._id,
                products: userBasket.data.products,
              })
              .then(() => {
                Toast.show({
                  type: "success",
                  text1: `${product.name} a été ajouté au panier`,
                  visibilityTime: 3000,
                });

                setResetBasket((prevVal) => !prevVal);
              })
              .then(() => {
                resetBasketFromChild(resetBasket);
                queryClient.invalidateQueries({
                  queryKey: ["basket-by-user-id"],
                });
              })
              .catch(() => {
                Toast.show({
                  type: "error",
                  text1: "Erreur lors de l'ajout au panier",
                  visibilityTime: 3000,
                });
              });
            return;
          }
        }

        const newProducts: Basket = {
          userId: user._id,
          products: userBasket.data.products,
        };

        newProducts.products.push(contentObject);

        updateBasket
          .mutateAsync({
            userId: newProducts.userId,
            products: newProducts.products,
          })
          .then(() => {
            Toast.show({
              type: "success",
              text1: `${product.name} a été ajouté au panier`,
              visibilityTime: 3000,
            });
          })
          .then(() => {
            resetBasketFromChild(!resetBasket);
            setResetBasket((prevVal) => !prevVal);
            queryClient.invalidateQueries({ queryKey: ["basket-by-user-id"] });
          })
          .catch(() => {
            Toast.show({
              type: "error",
              text1: "Erreur lors de l'ajout au panier",
              visibilityTime: 3000,
            });
          });
      } else {
        const productObject: Basket = {
          userId: user._id,
          products: [
            {
              idContent: product.id,
              contentName: product.name,
              quantity: 1,
            },
          ],
        };

        await addToBasket
          .mutateAsync({
            userId: productObject.userId,
            products: productObject.products,
          })
          .then(() => {
            Toast.show({
              type: "success",
              text1: `${product.name} a été ajouté au panier`,
              visibilityTime: 3000,
            });
          })
          .then(() => {
            resetBasketFromChild(!resetBasket);
            setResetBasket((prevVal) => !prevVal);
            queryClient.invalidateQueries({ queryKey: ["basket-by-user-id"] });
          })
          .catch(() => {
            Toast.show({
              type: "error",
              text1: "Erreur lors de l'ajout au panier",
              visibilityTime: 3000,
            });
          });
      }
    } else {
      let nbProduct = 1; //Update avec le dropdown
      let productToAdd = {
        idContent: product.id,
        contentName: product.name ?? "",
        quantity: Number(nbProduct),
      };

      let productsString = (await AsyncStorage.getItem("product")) as string;
      //Si le panier n'est pas vide
      if (productsString !== null) {
        let products = JSON.parse(productsString);
        //Si le produit existe déjà dans le panier on ajoute la quantité demandée
        for (let i = 0; i < products.length; i++) {
          if (products[i].idContent == product.id) {
            products[i].quantity = products[i].quantity + 1;
            AsyncStorage.setItem("product", JSON.stringify(products));
            setResetBasket((prevVal) => !prevVal);
            resetBasketFromChild(resetBasket);
            Toast.show({
              type: "success",
              text1: `${product.name} a été ajouté au panier`,
              visibilityTime: 3000,
            });
            return;
          }
        }

        //Ajout d'un produit supplémentaire dans le panier
        products.push(productToAdd);
        AsyncStorage.setItem("product", JSON.stringify(products));
        setResetBasket((prevVal) => !prevVal);
        resetBasketFromChild(resetBasket);
      } else {
        let products = [];
        products.push(productToAdd);
        AsyncStorage.setItem("product", JSON.stringify(products));
        setResetBasket((prevVal) => !prevVal);
        resetBasketFromChild(resetBasket);
      }
      Toast.show({
        type: "success",
        text1: `${product.name} a été ajouté au panier`,
        visibilityTime: 3000,
      });
    }
  };
  return (
    <StyledView className="h-72 w-full bg-secondary rounded-lg mb-8 relative">
      <StyledTouchable
        className="h-5/6"
        onPress={() => navigate(`/product/${product.id}`)}
      >
        <Image
          source={{ uri: product.image }}
          style={{
            width: "100%",
            height: "100%",
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />
      </StyledTouchable>
      <StyledView className="flex flex-row justify-between items-center h-1/6 px-4">
        <StyledText className="text-black font-medium">
          {product.name}
        </StyledText>
        <StyledView className="text-black font-bold flex flex-row">
          <StyledText className="mr-2 p-2">1 ^</StyledText>
          <StyledTouchable
            className="bg-primary rounded p-2"
            onPress={() => onPressCreateBasket(product)}
          >
            <StyledText>Ajouter</StyledText>
          </StyledTouchable>
        </StyledView>
      </StyledView>
      <StyledView className="absolute h-12 flex justify-center items-center -top-4 -right-3 bg-primary rounded-lg rounded-bl-none">
        <StyledText className="p-1 text-base"> {product.price}€</StyledText>
      </StyledView>
    </StyledView>
  );
};

export default ProductCard;
