"use client";

import { PropsWithChildren, useEffect } from "react";
import { AuthStatus } from "../../types/auth/auth_status";
import { View, Text } from "react-native";
import { styled } from "nativewind";
import { useAuth } from "../../hooks/useAuth";
import React from "react";

const StyledView = styled(View);

export const AuthProvider = ({ children }: PropsWithChildren) => {
  const { status, authenticate } = useAuth();

  useEffect(() => authenticate(), [authenticate]);

  if (!status) {
    return (
      <StyledView className="flex h-screen w-full items-center justify-center">
        <Text>Chargement</Text>
      </StyledView>
    );
  }

  return <>{children}</>;
};
