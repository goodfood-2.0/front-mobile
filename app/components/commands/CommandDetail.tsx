import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import { Text, ScrollView, View, TouchableOpacity } from "react-native";
import { Order, OrderContent } from "../../types/types";
import { CommandProduct } from "./CommandProduct";
import { useFetchRestaurantById } from "../../hooks/restaurants/use_fetch_restaurant_by_id";
import Loader from "../Loader";
import { useFetchDeliveriesByOrderID } from "../../hooks/delivery/use_fetch_delivery_by_order_id";
import { CommandsStatusDelivery } from "./CommandsStatusDelivery";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);

type CommandDetailProps = {
  order: Order;
};

export const CommandDetail = ({ order }: CommandDetailProps) => {
  const restaurant = useFetchRestaurantById(order.idRestaurant);

  const getFullPrice = (products: OrderContent[]) => {
    let total = 0;
    for (let i = 0; i < products.length; i++) {
      if (products[i].quantity > 1) {
        total += products[i].price * products[i].quantity;
      } else {
        total += products[i].price;
      }
    }
    return total.toFixed(2);
  };

  if (restaurant.isLoading || !order.id)
    return <StyledText>Chargement...</StyledText>;
  return (
    <StyledView className="w-full border-b border-b-secondary py-8 flex flex-row">
      <StyledView className="flex flex-col justify-center items-center mr-8">
        <StyledText className="text-base mb-1">
          {new Date(order.createdAt).toLocaleDateString()}
        </StyledText>
        <CommandsStatusDelivery idOrder={order.id} />
      </StyledView>
      <StyledView className="flex flex-col">
        <StyledText className="mb-2 text-base">
          {restaurant.data?.name}
        </StyledText>
        <StyledView className="flex flex-row w-full">
          <StyledView>
            {order.orderContents.map((content) => {
              return (
                <CommandProduct
                  key={content.idContent}
                  idContent={content.idContent}
                  quantity={content.quantity}
                />
              );
            })}
          </StyledView>
        </StyledView>
      </StyledView>
      <StyledView className="flex justify-end items-end w-max">
        <StyledText className="text-base text-primary">
          {getFullPrice(order.orderContents)}€
        </StyledText>
      </StyledView>
    </StyledView>
  );
};
