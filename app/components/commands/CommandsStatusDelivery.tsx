import { useFetchDeliveriesByOrderID } from "../../hooks/delivery/use_fetch_delivery_by_order_id";
import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import { Text, ScrollView, View, TouchableOpacity } from "react-native";
import Loader from "../Loader";
import { useNavigate } from "react-router-native";
import { Feather } from "@expo/vector-icons";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);

type CommandsStatusDeliveryProps = {
  idOrder: number;
};

export const CommandsStatusDelivery = ({
  idOrder,
}: CommandsStatusDeliveryProps) => {
  const delivery = useFetchDeliveriesByOrderID(idOrder);
  const [styleStatus, setStyleStatus] = useState<string>("");
  const navigate = useNavigate();

  useEffect(() => {
    if (!delivery.data) return;
    switch (delivery.data[0].status.myStatus) {
      case "En cours":
        setStyleStatus("bg-orange-300 hover:bg-orange-400");
        break;
      case "Livré":
        setStyleStatus("bg-green-300 hover:bg-green-400");
        break;
      default:
        setStyleStatus("bg-red-500 hover:bg-red-400");
        break;
    }
  }, [delivery.data]);

  if (delivery.isLoading) return <StyledText>"Chargement..."</StyledText>;
  return (
    <StyledTouchable
      className={`flex p-2 ${styleStatus} rounded-md text-black cursor-pointer animate-spin`}
      onPress={() => navigate(`/command/${idOrder}`)}
    >
      {delivery.data && delivery.data[0].status.myStatus == "Livré" ? (
        <Feather name="check" size={24} color="black" />
      ) : (
        <Feather name="truck" size={24} color="black" />
      )}
    </StyledTouchable>
  );
};
