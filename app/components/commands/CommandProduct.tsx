import { styled } from "nativewind";
import React from "react";
import { Text } from "react-native";
import { useFetchProductByID } from "../../hooks/catalog/product/use_fetch_product_by_id";
import Loader from "../Loader";

const StyledText = styled(Text);

type CommandProductProps = {
  idContent: number;
  quantity: number;
};

export const CommandProduct = ({
  idContent,
  quantity,
}: CommandProductProps) => {
  const product = useFetchProductByID(idContent);

  if (product.isLoading) return <Loader />;
  return (
    <StyledText className="mb-1">
      - {product.data?.name} {quantity > 0 && `x${quantity}`}
    </StyledText>
  );
};
