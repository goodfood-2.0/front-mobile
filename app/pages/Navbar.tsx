import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { useLocation, useNavigate } from "react-router-native";
import { FontAwesome } from "@expo/vector-icons";
import { useAuth } from "../hooks/useAuth";
import { useFetchBasketByUserID } from "../hooks/basket/use_fetch_basket_by_id";
import AsyncStorage from "@react-native-async-storage/async-storage";

type NavbarProps = {
  resetBasket?: boolean;
};

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTouchable = styled(TouchableOpacity);

const Navbar = ({ resetBasket }: NavbarProps) => {
  const { status, user } = useAuth();

  const path = useLocation();
  const navigate = useNavigate();
  const userBasket = useFetchBasketByUserID(user ? user._id : "");

  const [countBasket, setCountBasket] = useState<number>(0);

  const [pathName, setPathName] = useState(path.pathname);

  const getBasket = async () => {
    if (user) {
      if (userBasket.data) {
        setCountBasket(userBasket.data.products.length);
      }
    } else {
      let productsString = (await AsyncStorage.getItem("product")) as string;
      if (productsString !== null) {
        let products = JSON.parse(productsString);
        setCountBasket(products.length);
      }
    }
  };

  useEffect(() => {
    setPathName(path.pathname);
  }, [path.pathname]);

  useEffect(() => {
    getBasket();
  }, [user, status, resetBasket, userBasket.data]);

  return (
    <StyledView className="w-full flex flex-row bg-secondary h-20 shadow-boxShadowTop">
      <StyledTouchable
        className="w-1/4 flex justify-center items-center"
        onPress={() => navigate("/")}
      >
        {pathName === "/" ? (
          <Ionicons name="home-sharp" size={24} color="black" />
        ) : (
          <Ionicons name="home-outline" size={24} color="black" />
        )}
      </StyledTouchable>
      <StyledTouchable
        className="w-1/4 flex justify-center items-center relative"
        onPress={() => navigate("/basket")}
      >
        {pathName === "/basket" ? (
          <Ionicons name="basket" size={28} color="black" />
        ) : (
          <Ionicons name="basket-outline" size={28} color="black" />
        )}
        {countBasket > 0 && (
          <StyledView className="absolute bg-primary h-4 w-4 top-1/4 right-1/4 rounded-full flex items-center justify-center">
            <StyledText className="text-xs">{countBasket}</StyledText>
          </StyledView>
        )}
      </StyledTouchable>
      <StyledTouchable
        className="w-1/4 flex justify-center items-center"
        onPress={() => navigate("/commands")}
      >
        {pathName.includes("command") ? (
          <FontAwesome name="inbox" size={24} color="black" />
        ) : (
          <Feather name="inbox" size={24} color="black" />
        )}
      </StyledTouchable>
      <StyledTouchable
        className="w-1/4 flex justify-center items-center"
        onPress={() =>
          status === 1 ? navigate("/profile") : navigate("/connect")
        }
      >
        {pathName === "/profile" ? (
          <FontAwesome name="user" size={24} color="black" />
        ) : (
          <FontAwesome name="user-o" size={24} color="black" />
        )}
      </StyledTouchable>
    </StyledView>
  );
};

export default Navbar;
