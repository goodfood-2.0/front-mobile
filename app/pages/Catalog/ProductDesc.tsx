import { styled } from "nativewind";
import React, { useState } from "react";
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import { useParams } from "react-router-native";
import Header from "../Header";
import Navbar from "../Navbar";
import { useFetchProductByID } from "../../hooks/catalog/product/use_fetch_product_by_id";
import Loader from "../../components/Loader";
import { Entypo } from "@expo/vector-icons";
import { useFetchIngredientsIdsByProductID } from "../../hooks/catalog/ingredients/use_fetch_ingredients_by_product_id";
import { IngredientsList } from "../../components/utils/IngredientsList";
import { useCreateBasket } from "../../hooks/basket/use_create_basket";
import { useAuth } from "../../hooks/useAuth";
import Toast from "react-native-toast-message";
import { Basket, Product } from "../../types/types";
import { useFetchBasketByUserID } from "../../hooks/basket/use_fetch_basket_by_id";
import { useUpdateBasket } from "../../hooks/basket/use_fetch_update_basket";
import AsyncStorage from "@react-native-async-storage/async-storage";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTouchable = styled(TouchableOpacity);
const StyledScrollView = styled(ScrollView);

const ProductDesc = () => {
  const { idProduct } = useParams();
  const { user } = useAuth();

  const addToBasket = useCreateBasket();
  const updateBasket = useUpdateBasket();
  const userBasket = useFetchBasketByUserID(user ? user._id : "");

  const [openDesc, setOpenDesc] = useState(false);
  const [openIngredients, setOpenIngredients] = useState(false);
  const [resetBasket, setResetBasket] = useState<boolean>(false);

  const product = useFetchProductByID(Number(idProduct));
  const productIngredients = useFetchIngredientsIdsByProductID(
    Number(idProduct)
  );

  const onPressCreateBasket = async (product: Product) => {
    //AJOUTER LA QUANTITE QUAND ON AURA LE DROPDOWN

    const contentObject = {
      idContent: product.ID,
      contentName: product.name,
      quantity: 1,
    };

    if (user) {
      if (userBasket.data) {
        for (let i = 0; i < userBasket.data.products.length; i++) {
          if (userBasket.data.products[i].idContent == product.ID) {
            userBasket.data.products[i].quantity += 1;
            updateBasket
              .mutateAsync({
                userId: user._id,
                products: userBasket.data.products,
              })
              .then(() => {
                Toast.show({
                  type: "success",
                  text1: `${product.name} a été ajouté au panier`,
                  visibilityTime: 3000,
                });
              })
              .then(() => {
                setResetBasket((prevVal) => !prevVal);
              })
              .catch(() => {
                Toast.show({
                  type: "error",
                  text1: "Erreur lors de l'ajout au panier",
                  visibilityTime: 3000,
                });
              });
            return;
          }
        }

        const newProducts: Basket = {
          userId: user._id,
          products: userBasket.data.products,
        };

        newProducts.products.push(contentObject);

        updateBasket
          .mutateAsync({
            userId: newProducts.userId,
            products: newProducts.products,
          })
          .then(() => {
            Toast.show({
              type: "success",
              text1: `${product.name} a été ajouté au panier`,
              visibilityTime: 3000,
            });
          })
          .then(() => {
            setResetBasket((prevVal) => !prevVal);
          })
          .catch(() => {
            Toast.show({
              type: "error",
              text1: "Erreur lors de l'ajout au panier",
              visibilityTime: 3000,
            });
          });
      } else {
        const productObject: Basket = {
          userId: user._id,
          products: [
            {
              idContent: product.ID,
              contentName: product.name,
              quantity: 1,
            },
          ],
        };

        await addToBasket
          .mutateAsync({
            userId: productObject.userId,
            products: productObject.products,
          })
          .then(() => {
            Toast.show({
              type: "success",
              text1: `${product.name} a été ajouté au panier`,
              visibilityTime: 3000,
            });
          })
          .then(() => {
            setResetBasket((prevVal) => !prevVal);
          })
          .catch(() => {
            Toast.show({
              type: "error",
              text1: "Erreur lors de l'ajout au panier",
              visibilityTime: 3000,
            });
          });
      }
    } else {
      let nbProduct = 1; //Update avec le dropdown
      let productToAdd = {
        idContent: product.ID,
        contentName: product.name ?? "",
        quantity: Number(nbProduct),
      };

      let productsString = (await AsyncStorage.getItem("product")) as string;
      //Si le panier n'est pas vide
      if (productsString !== null) {
        let products = JSON.parse(productsString);
        //Si le produit existe déjà dans le panier on ajoute la quantité demandée
        for (let i = 0; i < products.length; i++) {
          if (products[i].idContent == product.ID) {
            products[i].quantity = products[i].quantity + 1;
            AsyncStorage.setItem("product", JSON.stringify(products));
            setResetBasket((prevVal) => !prevVal);
            Toast.show({
              type: "success",
              text1: `${product.name} a été ajouté au panier`,
              visibilityTime: 3000,
            });
            return;
          }
        }
        //Ajout d'un produit supplémentaire dans le panier
        products.push(productToAdd);
        AsyncStorage.setItem("product", JSON.stringify(products));
      } else {
        let products = [];
        products.push(productToAdd);
        setResetBasket((prevVal) => !prevVal);
        AsyncStorage.setItem("product", JSON.stringify(products));
      }
      Toast.show({
        type: "success",
        text1: `${product.name} a été ajouté au panier`,
        visibilityTime: 3000,
      });
    }
  };

  if (product.isLoading || !product.data || productIngredients.isLoading) {
    return <Loader />;
  }
  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView className="flex flex-col w-full">
        <StyledView className="h-60">
          <Image
            source={{ uri: product.data?.image }}
            style={{
              width: "100%",
              height: "100%",
              objectFit: "cover",
            }}
          />
        </StyledView>
        <StyledView className="py-8 px-6 w-full flex flex-row justify-between items-center">
          <StyledText className="font-medium text-xl">
            {product.data?.name}
          </StyledText>
          <StyledText>Dropdown</StyledText>
        </StyledView>
        <StyledView className="bg-secondary h-full flex flex-col px-6">
          <StyledTouchable
            className="py-6 flex flex-col border-b border-zinc-300"
            onPress={() => setOpenIngredients((prevValue) => !prevValue)}
          >
            <StyledView className="flex flex-row justify-between items-center ">
              <StyledText className="text-lg font-medium">
                Composition du produit
              </StyledText>
              <Entypo
                name={openIngredients ? "chevron-up" : "chevron-down"}
                size={24}
                color="black"
              />
            </StyledView>
            {openIngredients && (
              <StyledView className="pl-6 mt-4">
                {productIngredients.data &&
                  productIngredients.data.data.map((ingredient) => (
                    <IngredientsList
                      key={ingredient.id_ingredient}
                      idIngredient={ingredient.id_ingredient}
                    />
                  ))}
              </StyledView>
            )}
          </StyledTouchable>
          <StyledTouchable
            onPress={() => setOpenDesc((prevValue) => !prevValue)}
            className="py-6 flex flex-col"
          >
            <StyledView className="flex flex-row justify-between items-center">
              <StyledText className="text-lg font-medium">
                Description du produit
              </StyledText>
              <Entypo
                name={openDesc ? "chevron-up" : "chevron-down"}
                size={24}
                color="black"
              />
            </StyledView>
            {openDesc && (
              <StyledView className="pl-6 mt-4">
                <StyledText>{product.data.description}</StyledText>
              </StyledView>
            )}
          </StyledTouchable>
          <StyledView className="mt-10 mb-6 w-full h-full flex items-center">
            <StyledTouchable
              className="bg-primary flex justify-center items-center p-2 rounded-lg w-full"
              onPress={() => onPressCreateBasket(product.data)}
            >
              <StyledText className="text-2xl shadow-xl">
                Ajouter au panier
              </StyledText>
            </StyledTouchable>
          </StyledView>
        </StyledView>
      </StyledScrollView>
      <Navbar resetBasket={resetBasket} />
    </StyledView>
  );
};

export default ProductDesc;
