import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import { styled } from "nativewind";
import Header from "./Header";
import Navbar from "./Navbar";
import { useAuth } from "../hooks/useAuth";
import { FontAwesome } from "@expo/vector-icons";
import Loader from "../components/Loader";
import {
  MaterialIcons,
  MaterialCommunityIcons,
  Entypo,
  Feather,
} from "@expo/vector-icons";
import { useNavigate } from "react-router-native";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTouchable = styled(TouchableOpacity);
const StyledScrollView = styled(ScrollView);

const Profile = () => {
  const { user, logout } = useAuth();
  const navigate = useNavigate();

  if (!user) {
    return <Loader />;
  }
  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView>
        <StyledView className="h-40 bg-lightBlack w-full flex flex-col items-center py-8">
          <StyledView className="h-14 w-14 rounded-full p-2 bg-secondary flex justify-center items-center">
            <FontAwesome name="user" size={32} color="#222222" />
          </StyledView>
          <StyledView className="mt-3">
            <StyledText className="text-white text-lg tracking-wide">
              {user.firstname.toUpperCase()}{" "}
              <StyledText className="text-primary text-lg font-bold tracking-wide">
                {user.lastname.toUpperCase()}
              </StyledText>
            </StyledText>
          </StyledView>
        </StyledView>
        <StyledView className="py-20 px-12">
          <StyledTouchable
            className="flex flex-row justify-between mb-6"
            onPress={() => navigate("/commands")}
          >
            <StyledView className="flex flex-row items-center">
              <Feather name="inbox" size={24} color="black" />
              <StyledText className="ml-4 text-base font-medium">
                Historique des commandes
              </StyledText>
            </StyledView>
            <Entypo name="chevron-thin-right" size={24} color="black" />
          </StyledTouchable>
          <StyledTouchable className="flex flex-row justify-between mb-6">
            <StyledView className="flex flex-row items-center">
              <MaterialCommunityIcons
                name="information-outline"
                size={24}
                color="black"
              />
              <StyledText className="ml-4 text-base font-medium">
                A propos de nous
              </StyledText>
            </StyledView>
            <Entypo name="chevron-thin-right" size={24} color="black" />
          </StyledTouchable>
          <StyledTouchable
            className="flex flex-row justify-between"
            onPress={() => logout(navigate)}
          >
            <StyledView className="flex flex-row items-center">
              <MaterialIcons name="logout" size={24} color="black" />
              <StyledText className="ml-4 text-base font-medium">
                Déconnexion
              </StyledText>
            </StyledView>
            <Entypo name="chevron-thin-right" size={24} color="black" />
          </StyledTouchable>
        </StyledView>
      </StyledScrollView>
      <Navbar />
    </StyledView>
  );
};

export default Profile;

//6304 code Karl
