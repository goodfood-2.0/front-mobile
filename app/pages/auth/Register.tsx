import React, { useState } from "react";
import {
  View,
  Image,
  Button,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useAuth } from "../../hooks/useAuth";
import { useNavigate } from "react-router-native";
import { styled } from "nativewind";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTextInput = styled(TextInput);
const StyledButton = styled(Button);
const StyledTouchableOpacity = styled(TouchableOpacity);

const Register = () => {
  const navigate = useNavigate();
  const { register } = useAuth();

  const [name, setName] = useState("");
  const [firstname, setFirstname] = useState("");
  const [email, setEmail] = useState("");

  return (
    <StyledView className="flex h-screen pt-6 items-center w-screen bg-secondary px-6">
      <StyledView className="p-8">
        <Image
          source={require("../../../assets/logoGF.png")}
          style={{
            width: 125,
            height: 125,
          }}
        />
      </StyledView>
      <StyledView className="flex flex-col w-full">
        <StyledView className="flex flex-col">
          <StyledText className="text-black font-thin">Nom</StyledText>
          <StyledTextInput
            className="border-0 border-b border-gray-400 w-full h-8 mb-4"
            onChangeText={(text) => setName(text)}
            value={name}
          />
        </StyledView>
        <StyledView className="flex flex-col">
          <StyledText className="text-black font-thin">Prénom</StyledText>
          <StyledTextInput
            className="border-0 border-b border-gray-400 w-full h-8"
            onChangeText={(text) => setFirstname(text)}
            value={firstname}
          />
        </StyledView>
        <StyledView className="flex flex-col mb-10 mt-4">
          <StyledText className="text-black font-thin">Email</StyledText>
          <StyledTextInput
            className="border-0 border-b border-gray-400 w-full h-8"
            onChangeText={(text) => setEmail(text)}
            value={email}
          />
        </StyledView>
        <StyledView className="flex flex-col w-full items-center">
          <StyledTouchableOpacity
            className="text-black bg-primary mb-4 p-2 rounded w-full flex justify-center items-center"
            onPress={() => register(name, firstname, email, navigate)}
          >
            <StyledText className="text-lg font-semibold">
              S'inscrire
            </StyledText>
          </StyledTouchableOpacity>
          <StyledTouchableOpacity
            className="text-black bg-secondary border border-primary p-2 rounded w-full flex justify-center items-center"
            onPress={() => navigate("/connect")}
          >
            <StyledText className="text-lg font-semibold">Connexion</StyledText>
          </StyledTouchableOpacity>
        </StyledView>
      </StyledView>
    </StyledView>
  );
};

export default Register;
