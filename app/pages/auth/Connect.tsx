import React, { useState } from "react";
import {
  View,
  Image,
  Button,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useAuth } from "../../hooks/useAuth";
import { useNavigate } from "react-router-native";
import { styled } from "nativewind";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledTextInput = styled(TextInput);
const StyledButton = styled(Button);
const StyledTouchableOpacity = styled(TouchableOpacity);

const Connect = () => {
  const navigate = useNavigate();
  const { login } = useAuth();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <StyledView className="flex h-screen pt-6 items-center w-screen bg-secondary px-6">
      <StyledView className="p-8">
        <Image
          source={require("../../../assets/logoGF.png")}
          style={{
            width: 125,
            height: 125,
          }}
        />
      </StyledView>
      <StyledView className="flex flex-col w-full mt-4">
        <StyledView className="flex flex-col">
          <StyledText className="text-black font-thin">Email</StyledText>
          <StyledTextInput
            className="border-0 border-b border-zinc-400 w-full h-8 mb-4"
            onChangeText={(text) => setEmail(text)}
            value={email}
            autoCapitalize="none"
          />
        </StyledView>
        <StyledView className="flex flex-col mb-6">
          <StyledText className="text-black font-thin">Mot de passe</StyledText>
          <StyledTextInput
            className="border-0 border-b border-zinc-400 w-full h-8"
            onChangeText={(text) => setPassword(text)}
            value={password}
            autoCapitalize="none"
            secureTextEntry
          />
        </StyledView>
        <StyledView className="flex flex-col w-full items-center mt-10">
          <StyledTouchableOpacity
            className="text-black bg-primary mb-4 p-2 rounded-lg w-full flex justify-center items-center"
            onPress={() => login(email, password, navigate)}
          >
            <StyledText className="text-lg font-semibold">Connexion</StyledText>
          </StyledTouchableOpacity>
          <StyledTouchableOpacity
            className="text-black bg-secondary border border-primary p-2 rounded-lg w-full flex justify-center items-center"
            onPress={() => navigate("/register")}
          >
            <StyledText className="text-lg font-semibold">
              Inscription
            </StyledText>
          </StyledTouchableOpacity>
        </StyledView>
        <StyledView className="mt-10 w-full">
          <StyledTouchableOpacity
            className="w-full flex justify-center items-center"
            onPress={() => navigate("/")}
          >
            <StyledText className="text-sm font-extralight">
              Retourner à l'accueil ? Cliquez ici
            </StyledText>
          </StyledTouchableOpacity>
        </StyledView>
      </StyledView>
    </StyledView>
  );
};

export default Connect;
