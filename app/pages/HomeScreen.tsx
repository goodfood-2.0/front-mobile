import { styled } from "nativewind";
import React, { useState, useEffect } from "react";
import { View, Text, Button, ScrollView } from "react-native";
import { useFetchAllProducts } from "../hooks/catalog/product/use_fetch_all_products";
import Navbar from "./Navbar";
import Header from "./Header";
import ProductCard from "../components/catalog/ProductCard";
import Loader from "../components/Loader";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);

const HomeScreen = () => {
  const products = useFetchAllProducts();

  const [resetBasket, setResetBasket] = useState<boolean>(false);

  const getBooleanFromChild = (n: boolean) => {
    setResetBasket(n);
  };

  if (products.isLoading) {
    return <Loader />;
  }

  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView>
        <StyledText className="text-xl mt-4 px-4 pr-20">
          Une sélection de produits, rien que{" "}
          <StyledText className="text-primary">pour vous</StyledText>
        </StyledText>
        <StyledView className="mt-8 flex flex-col px-6">
          {products.data &&
            products.data.data.map((product) => (
              <ProductCard
                key={product.id}
                product={product}
                resetBasketFromChild={getBooleanFromChild}
              />
            ))}
        </StyledView>
      </StyledScrollView>
      <Navbar resetBasket={resetBasket} />
    </StyledView>
  );
};

export default HomeScreen;
