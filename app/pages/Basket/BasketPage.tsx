import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import {
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import Header from "../Header";
import Navbar from "../Navbar";
import { useAuth } from "../../hooks/useAuth";
import { useFetchBasketByUserID } from "../../hooks/basket/use_fetch_basket_by_id";
import { ProductBasket } from "../../types/types";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BasketList from "../../components/basket/BasketList";
import { useUpdateBasket } from "../../hooks/basket/use_fetch_update_basket";
import Toast from "react-native-toast-message";
import { useNavigate } from "react-router-native";
import { useCreateBasket } from "../../hooks/basket/use_create_basket";
import { FontAwesome5 } from "@expo/vector-icons";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);
const StyledTextInput = styled(TextInput);

const BasketPage = () => {
  //TO DO, si un panier existe en async storage, le récupérer et créer un panier en base de données dans useAuth
  const { user, status } = useAuth();
  const updateBasket = useUpdateBasket();
  const createBasket = useCreateBasket();

  const navigate = useNavigate();

  const [basket, setBasket] = useState<ProductBasket[]>([]);
  const [resetBasket, setResetBasket] = useState<boolean>(false);
  const [addressClient, setAddressClient] = useState("");

  const [mailUserDisconnected, setMailUserDisconnected] = useState("");
  const [placeHolderMailUser, setPlaceHolderMailUser] = useState("");

  const onChangeSetEmailUser = (emailUser: string) => {
    setMailUserDisconnected(emailUser);
    AsyncStorage.setItem("emailUser", emailUser);
  };

  const getEmailUser = async () => {
    const emailUser = await AsyncStorage.getItem("emailUser");
    if (emailUser) {
      setPlaceHolderMailUser(emailUser);
    }
  };

  const getUserAddress = async () => {
    const address = await AsyncStorage.getItem("address");
    if (address) {
      setAddressClient(address);
    }
  };

  const userBasket = useFetchBasketByUserID(user ? user._id : "");

  const deleteFromBasket = async (id: number) => {
    if (user && userBasket.data) {
      const products = userBasket.data.products;
      for (let i = 0; i < products.length; i++) {
        if (products[i].idContent === id) {
          userBasket.data.products.splice(i, 1);
          updateBasket
            .mutateAsync({
              userId: userBasket.data.userId,
              products: userBasket.data.products,
            })
            .then(() => {
              Toast.show({
                type: "success",
                text1: "Produit supprimé du panier",
                visibilityTime: 3000,
              });
            });
          setResetBasket((prevVal) => !prevVal);
          setBasket(userBasket.data.products);
          return;
        }
      }
    } else {
      let productsString = (await AsyncStorage.getItem("product")) as string;
      if (productsString !== null) {
        let products = JSON.parse(productsString);
        for (let i = 0; i < products.length; i++) {
          if (products[i].idContent === id) {
            products.splice(i, 1);
            AsyncStorage.setItem("product", JSON.stringify(products));
            Toast.show({
              type: "success",
              text1: "Produit supprimé du panier",
              visibilityTime: 3000,
            });
            setResetBasket((prevVal) => !prevVal);
            setBasket(products);
            return;
          }
        }
      }
    }
  };

  const getBasket = async () => {
    if (user) {
      if (userBasket.data) {
        setBasket(userBasket.data.products);
      }
    } else {
      let productsString = (await AsyncStorage.getItem("product")) as string;
      if (productsString !== null) {
        let products = JSON.parse(productsString);
        setBasket(products);
      }
    }
  };

  //A tester
  const confirmCommand = () => {
    if (addressClient == "") {
      Toast.show({
        type: "error",
        text1: `Veuillez renseigner votre adresse de livraison`,
        visibilityTime: 3000,
      });
    } else {
      if (user) {
        navigate("/validate");
      } else {
        const products = basket.map((val) => {
          return {
            idContent: val.idContent,
            contentName: val.contentName,
            quantity: val.quantity,
          };
        });
        const orderBody = {
          userId:
            mailUserDisconnected !== ""
              ? mailUserDisconnected
              : placeHolderMailUser,
          products: products,
        };
        createBasket
          .mutateAsync({
            userId: orderBody.userId,
            products: orderBody.products,
          })
          .then(() => {
            navigate("/validate");
          });
      }
    }
  };

  useEffect(() => {
    getBasket();
    getUserAddress();
    getEmailUser();
  }, [user, status]);

  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView className="px-4 pt-4">
        <StyledView className="pb-6">
          <StyledText className="text-xl">
            Votre <StyledText className="text-primary">Panier</StyledText>
          </StyledText>
        </StyledView>
        <StyledView className="w-full flex justify-center items-center">
          <StyledView className="h-px w-5/6 bg-zinc-300" />
        </StyledView>
        <StyledView className="mb-6">
          {basket && basket.length > 0 ? (
            basket.map((product) => (
              <BasketList
                key={product.idContent}
                productBasket={product}
                deleteFromBasket={deleteFromBasket}
              />
            ))
          ) : (
            <StyledView className="w-full py-20 flex justify-center items-center">
              <StyledText className="text-2xl">
                Votre panier est vide
              </StyledText>
            </StyledView>
          )}
          {!user && basket && basket.length > 0 && (
            <StyledView className="bg-secondary w-11/12 ml-4 flex flex-row p-2 rounded-full items-center mt-10 shadow-sm">
              <FontAwesome5 name="user-alt" size={20} color="black" />
              <StyledTextInput
                className="w-full ml-3 h-8 p-0 text-xs"
                onChangeText={(text) => onChangeSetEmailUser(text)}
                placeholder={
                  placeHolderMailUser
                    ? placeHolderMailUser
                    : "Saisissez votre email"
                }
                value={mailUserDisconnected}
              />
            </StyledView>
          )}
          {basket && basket.length > 0 && (
            <StyledView className="mb-6 mt-4 px-4">
              <StyledTouchable
                className="w-full bg-primary flex items-center justify-center rounded-lg p-3 shadow-md"
                onPress={confirmCommand}
              >
                <StyledText className="text-xl font-bold">Commander</StyledText>
              </StyledTouchable>
            </StyledView>
          )}
        </StyledView>
      </StyledScrollView>
      <Navbar resetBasket={resetBasket} />
    </StyledView>
  );
};

export default BasketPage;
