import React, { useState } from "react";
import { View, Text, Button, TextInput } from "react-native";
import { useAuth } from "../hooks/useAuth";
import { useNavigate } from "react-router-native";
import { styled } from "nativewind";
import Navbar from "./Navbar";

const StyledView = styled(View);

const LocalStorage = ({ navigation }: any) => {
  const navigate = useNavigate();
  const { login } = useAuth();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <StyledView className="flex h-screen w-screen items-center justify-center bg-red-500">
      <StyledView className="flex flex-col gap-10">
        <TextInput
          style={{
            height: 40,
            width: 200,
            borderColor: "gray",
            borderWidth: 1,
          }}
          onChangeText={(text) => setEmail(text)}
          value={email}
        />
        <TextInput
          style={{
            height: 40,
            width: 200,
            borderColor: "gray",
            borderWidth: 1,
          }}
          onChangeText={(text) => setPassword(text)}
          value={password}
        />
        <Button title="Lol" onPress={() => login(email, password, navigate)} />
      </StyledView>
      <Navbar />
    </StyledView>
  );
};

export default LocalStorage;
