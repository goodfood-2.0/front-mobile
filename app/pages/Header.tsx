import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Text,
  ScrollView,
} from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import { Fontisto } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { useNavigate } from "react-router-native";
import { useFetchAddresses } from "../hooks/geoapify/use_fetch_adresses";
import AsyncStorage from "@react-native-async-storage/async-storage";

const StyledView = styled(View);
const StyledTouchable = styled(TouchableOpacity);
const StyledTextInput = styled(TextInput);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

const Header = () => {
  const navigate = useNavigate();
  const fetchAddresses = useFetchAddresses();
  const [showAddress, setShowAddress] = useState(false);
  const [showSearch, setShowSearch] = useState(false);

  const [address, setAddress] = useState("");
  const [placeHolderAddress, setPlaceHolderAddress] = useState("");
  const [addressesFetched, setAddressesFetched] = useState([]);

  const getUserAddress = async () => {
    const address = await AsyncStorage.getItem("address");
    if (address) {
      setPlaceHolderAddress(address);
    }
  };

  const onPressSetAddress = (address: string) => {
    setAddress(address);
    setAddressesFetched([]);
    AsyncStorage.setItem("address", address);
    setShowAddress(false);
  };

  useEffect(() => {
    if (address.length > 2) {
      sleep(2000);
      fetchAddresses.mutateAsync(address).then((data: any) => {
        setAddressesFetched(data.results);
      });
    } else {
      setAddressesFetched([]);
    }
  }, [address]);

  useEffect(() => {
    getUserAddress();
  });

  return (
    <StyledView className="w-full h-32 bg-secondary pt-14 shadow-boxShadowBottom z-50">
      <StyledView className="flex flex-row justify-around h-full">
        {showAddress ? (
          <StyledView className="mt-4 px-[25px]">
            <StyledView
              className={`flex flex-row justify-around gap-1 w-full h-12 bg-gray ${
                addressesFetched.length > 0 ? "rounded-t-xl" : "rounded-xl"
              } relative`}
            >
              <StyledTouchable
                className="h-10 flex justify-center items-center w-10 bg-black rounded-full"
                onPress={() => setShowAddress(false)}
              >
                <FontAwesome5 name="map-marker-alt" size={20} color="#E9E9E9" />
              </StyledTouchable>
              <StyledView className="w-4/6 h-full">
                <StyledTextInput
                  className="w-full rounded-r-full -mt-1 h-full p-0"
                  onChangeText={(text) => setAddress(text)}
                  placeholder={
                    placeHolderAddress
                      ? placeHolderAddress
                      : "Rechercher une adresse..."
                  }
                  value={address}
                />
              </StyledView>
              <StyledTouchable
                className="h-10 flex justify-center items-center w-10 rounded-full"
                onPress={() => setShowAddress(false)}
              >
                <AntDesign name="close" size={24} color="black" />
              </StyledTouchable>
              {addressesFetched.length > 0 && (
                <StyledScrollView className="absolute top-10 bg-gray -left-1 w-full h-max max-h-52 rounded-b-xl px-2">
                  {addressesFetched.map((address: any, index: number) => {
                    const nameAddress = `${address.address_line1}, ${address.address_line2}`;
                    return (
                      <StyledTouchable
                        key={index}
                        className="p-2 border-b border-zinc-300"
                        onPress={() => onPressSetAddress(nameAddress)}
                      >
                        <StyledText>{nameAddress}</StyledText>
                      </StyledTouchable>
                    );
                  })}
                </StyledScrollView>
              )}
            </StyledView>
          </StyledView>
        ) : showSearch ? (
          <StyledView className="mt-4 px-[21px]">
            <StyledView className="flex flex-row justify-around gap-1 w-full h-12 bg-gray rounded-xl ">
              <StyledTouchable
                className="h-10 flex justify-center items-center w-10 rounded-full"
                onPress={() => setShowSearch(false)}
              >
                <AntDesign name="close" size={24} color="black" />
              </StyledTouchable>
              <StyledView className="w-4/6 h-full">
                <StyledTextInput
                  className="w-full rounded-r-full -mt-1 h-full p-0"
                  onChangeText={(text) => setAddress(text)}
                  placeholder="Rechercher un produit..."
                  value={address}
                />
              </StyledView>
              <StyledTouchable
                className="h-10 flex justify-center items-center w-10 rounded-full bg-black"
                onPress={() => setShowSearch(false)}
              >
                <Fontisto name="search" size={20} color="#E9E9E9" />
              </StyledTouchable>
            </StyledView>
          </StyledView>
        ) : (
          <>
            <StyledView className="flex justify-center items-center w-1/4">
              <StyledTouchable
                className="p-2 h-10 flex justify-center items-center w-10 bg-lightBlack rounded-full"
                onPress={() => setShowAddress(true)}
              >
                <FontAwesome5 name="map-marker-alt" size={20} color="#F2F2F2" />
              </StyledTouchable>
            </StyledView>
            <StyledTouchable
              className="h-full flex justify-center items-center w-2/4"
              onPress={() => navigate("/")}
            >
              <Image
                source={require("../../assets/logoBlackPng.png")}
                style={{
                  width: 100,
                  height: 50,
                }}
              />
            </StyledTouchable>
            <StyledView className="flex justify-center items-center px-4 w-1/4">
              <StyledTouchable
                className="p-2 h-10 flex justify-center items-center w-10 bg-lightBlack rounded-full"
                onPress={() => setShowSearch(true)}
              >
                <Fontisto name="search" size={20} color="white" />
              </StyledTouchable>
            </StyledView>
          </>
        )}
      </StyledView>
    </StyledView>
  );
};

export default Header;
