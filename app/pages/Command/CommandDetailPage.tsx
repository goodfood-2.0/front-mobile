import { useParams } from "react-router-native";
import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import { Text, ScrollView, View, TouchableOpacity } from "react-native";
import Header from "../Header";
import Navbar from "../Navbar";
import { useFetchOrderById } from "../../hooks/order/use_fetch_order_by_id";
import Loader from "../../components/Loader";
import { useFetchDeliveriesByOrderID } from "../../hooks/delivery/use_fetch_delivery_by_order_id";
import { CommandDetailProduct } from "./CommandDetailProduct";
import { useFetchRestaurantById } from "../../hooks/restaurants/use_fetch_restaurant_by_id";
import { formatDate } from "../../utils/functions";
import { CommandsStatusDelivery } from "../../components/commands/CommandsStatusDelivery";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);

const CommandDetailPage = () => {
  const { idCommand } = useParams();

  const order = useFetchOrderById(Number(idCommand));
  const delivery = useFetchDeliveriesByOrderID(Number(idCommand));
  const restaurant = useFetchRestaurantById(order.data?.idRestaurant ?? -1);

  if (order.isLoading || delivery.isLoading || restaurant.isLoading)
    return <Loader />;
  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView className="px-4 pt-4">
        <StyledView className="pb-6">
          <StyledText className="text-xl">
            Détail de votre
            <StyledText className="text-primary"> Commande</StyledText>
          </StyledText>
        </StyledView>
        <StyledView className="w-full flex justify-center items-center">
          <StyledView className="h-px w-5/6 bg-zinc-300" />
        </StyledView>
        <StyledView className="mt-2 flex flex-col">
          <StyledText className="pt-4 text-lg font-bold">
            {restaurant.data?.name}
          </StyledText>
          <StyledText className="mb-3 text-base">
            Commande du{" "}
            {order.data && formatDate(new Date(order.data.createdAt))}
          </StyledText>
          {order.data &&
            order.data.orderContents.map((content) => {
              return (
                <CommandDetailProduct
                  key={content.idContent}
                  idProduct={content.idContent}
                  quantity={content.quantity}
                />
              );
            })}
        </StyledView>
        <StyledView className="h-72 w-full bg-secondary flex flex-col p-3">
          <StyledText className="px-4 font-medium">
            Livraison au{" "}
            {`${order.data?.address}, ${order.data?.zipCode} ${order.data?.city}`}
          </StyledText>
          <StyledView className="px-4 mt-4 flex flex-row items-center">
            <CommandsStatusDelivery idOrder={Number(idCommand)} />
            <StyledText className="ml-4 text-base">
              {delivery.data && delivery.data[0].status.myStatus}
            </StyledText>
          </StyledView>
          <StyledText>PUIS LA CARTE</StyledText>
        </StyledView>
      </StyledScrollView>
      <Navbar />
    </StyledView>
  );
};

export default CommandDetailPage;
