import { styled } from "nativewind";
import React from "react";
import { Image, Text, ScrollView, View, TouchableOpacity } from "react-native";
import { useFetchProductByID } from "../../hooks/catalog/product/use_fetch_product_by_id";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);

type CommandDetailProductProps = {
  idProduct: number;
  quantity: number;
};

export const CommandDetailProduct = ({
  idProduct,
  quantity,
}: CommandDetailProductProps) => {
  const product = useFetchProductByID(idProduct);

  if (product.isLoading) return <StyledText>Chargement...</StyledText>;
  return (
    <StyledView className="pb-4 mt-4 flex flex-row border-b border-secondary">
      <StyledTouchable className="h-32 w-32">
        <Image
          source={{ uri: product.data?.image }}
          style={{
            width: "100%",
            height: "100%",
            borderBottomLeftRadius: 8,
            borderTopLeftRadius: 8,
          }}
        />
      </StyledTouchable>
      <StyledView className="flex flex-col pl-2 mt-2">
        <StyledText className="text-base font-medium">
          {product.data?.name} {quantity > 1 && `x${quantity}`}
        </StyledText>
        <StyledText className="w-52 mt-1">
          {product.data?.description}
        </StyledText>
        <StyledView className="w-52 h-10 flex items-end justify-end">
          <StyledText className="text-primary">
            {product.data?.price}€
          </StyledText>
        </StyledView>
      </StyledView>
    </StyledView>
  );
};
