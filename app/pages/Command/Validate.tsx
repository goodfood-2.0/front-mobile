import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import {
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import Header from "../Header";
import Navbar from "../Navbar";
import { useAuth } from "../../hooks/useAuth";
import { useNavigate } from "react-router-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { OrderBody } from "../../types/types";
import { useValidateBasket } from "../../hooks/basket/use_validate_basket";
import { useQueryClient } from "@tanstack/react-query";
import { FontAwesome5 } from "@expo/vector-icons";
import Toast from "react-native-toast-message";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);

const Validate = () => {
  const { user } = useAuth();
  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const validateBasket = useValidateBasket();

  const [addressClient, setAddressClient] = useState("");
  const [mailUserDisconnected, setMailUserDisconnected] = useState("");

  const getEmailUser = async () => {
    const emailUser = await AsyncStorage.getItem("emailUser");
    if (emailUser) {
      setMailUserDisconnected(emailUser);
    }
  };

  const getUserAddress = async () => {
    const address = await AsyncStorage.getItem("address");
    if (address) {
      setAddressClient(address);
    }
  };

  useEffect(() => {
    getUserAddress();
    getEmailUser();
  }, [user]);

  const submitOrder = () => {
    const adresse = addressClient.split(",");
    const indexOfCity = adresse[1].trim().indexOf(" ");
    const zipCode = adresse[1].trim().substring(0, indexOfCity + 1);
    const city = adresse[1]
      .trim()
      .substring(indexOfCity + 1, adresse[1].length);

    if (user) {
      const orderBody: OrderBody = {
        email: user.email,
        idRestaurant: 1, //TRICHE JUSTE ICI A VOIR SI LE TEMPS D'AJOUTER L'ID RESTAURANT
        country: adresse[2],
        city: city,
        address: adresse[0],
        additionnalAddress: "",
        zipCode: zipCode.trim(),
        commandType: "client",
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      };

      const order = {
        userId: user._id,
        orderInfo: orderBody,
      };

      validateBasket
        .mutateAsync({
          userId: order.userId,
          orderInfo: order.orderInfo,
        })
        .then((data: any) => {
          queryClient.invalidateQueries({ queryKey: ["basket-by-user-id"] });
          navigate(`/command/${data.id}`);
        });
    } else {
      if (mailUserDisconnected !== "") {
        const orderBody: OrderBody = {
          email: mailUserDisconnected,
          idRestaurant: 1, //TRICHE JUSTE ICI A VOIR SI LE TEMPS D'AJOUTER L'ID RESTAURANT
          country: adresse[2],
          city: city,
          address: adresse[0],
          additionnalAddress: "",
          zipCode: zipCode.trim(),
          commandType: "client",
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
        };

        const order = {
          mailUser: mailUserDisconnected,
          orderInfo: orderBody,
        };

        validateBasket
          .mutateAsync({
            userId: order.mailUser,
            orderInfo: order.orderInfo,
          })
          .then((data: any) => {
            queryClient.invalidateQueries({ queryKey: ["basket-by-user-id"] });
            navigate(`/command/${data.id}`);
          });
      } else {
        Toast.show({
          type: "error",
          text1: "Veuillez saisir votre adresse mail",
          visibilityTime: 3000,
        });
      }
    }
  };

  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView className="px-4 pt-4">
        <StyledView className="pb-6">
          <StyledText className="text-xl">
            <StyledText className="text-primary">Paiement</StyledText>
          </StyledText>
        </StyledView>
        <StyledView className="w-full flex justify-center items-center">
          <StyledText className="w-10/12 text-base font-medium">
            Détail de la livraison
          </StyledText>
          <StyledView className="h-px w-5/6 bg-zinc-300" />
        </StyledView>
        <StyledView className="mt-6 mx-4">
          <StyledView className="bg-secondary w-full flex flex-row p-2 rounded-full items-center shadow-sm">
            <FontAwesome5 name="map-marker-alt" size={20} color="black" />
            <StyledText className="ml-3 text-xs">{addressClient}</StyledText>
          </StyledView>
        </StyledView>
        <StyledView className="my-6">
          <StyledTouchable
            className="w-full bg-black p-2 rounded-lg shadow-md"
            onPress={submitOrder}
          >
            <StyledText className="w-full text-center text-lg text-white font-bold">
              Paiement
            </StyledText>
          </StyledTouchable>
        </StyledView>
      </StyledScrollView>
      <Navbar />
    </StyledView>
  );
};

export default Validate;
