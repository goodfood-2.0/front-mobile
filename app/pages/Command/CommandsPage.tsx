import { styled } from "nativewind";
import React, { useEffect, useState } from "react";
import { Text, ScrollView, View, TouchableOpacity } from "react-native";
import Header from "../Header";
import Navbar from "../Navbar";
import { useAuth } from "../../hooks/useAuth";
import { useFetchOrdersByEmail } from "../../hooks/order/use_fetch_all_orders";
import Loader from "../../components/Loader";
import { CommandDetail } from "../../components/commands/CommandDetail";
import { AntDesign } from "@expo/vector-icons";
import { useNavigate } from "react-router-native";

const StyledView = styled(View);
const StyledText = styled(Text);
const StyledScrollView = styled(ScrollView);
const StyledTouchable = styled(TouchableOpacity);

const CommandsPage = () => {
  const { user } = useAuth();
  const navigate = useNavigate();

  const commands = useFetchOrdersByEmail(user?.email ?? "");

  if (commands.isLoading) return <Loader />;
  return (
    <StyledView className="h-screen w-full flex flex-col">
      <Header />
      <StyledScrollView className="px-4 pt-4">
        <StyledView className="pb-6">
          <StyledText className="text-xl">
            Historique des
            <StyledText className="text-primary"> Commandes</StyledText>
          </StyledText>
        </StyledView>
        <StyledView className="w-full flex justify-center items-center">
          <StyledView className="h-px w-5/6 bg-zinc-300" />
        </StyledView>
        <StyledView className="mt-6">
          {user ? (
            commands.data
              ?.sort(
                (a, b) =>
                  new Date(b.createdAt).getTime() -
                  new Date(a.createdAt).getTime()
              )
              .map((command) => (
                <CommandDetail key={command.id} order={command} />
              ))
          ) : (
            <StyledView className="flex flex-col justify-center items-center px-2">
              <StyledView className="mb-8 bg-black rounded-full p-3">
                <AntDesign name="user" size={40} color="white" />
              </StyledView>
              <StyledText className="text-center text-base mb-8">
                Veuillez vous connecter pour voir l'historique de vos commandes
              </StyledText>
              <StyledView className="h-px w-full bg-zinc-300 mb-8" />
              <StyledView>
                <StyledTouchable
                  className="bg-black p-3 rounded-lg shadow-button"
                  onPress={() => navigate("/connect")}
                >
                  <StyledText className="text-lg text-white font-bold">
                    Se connecter
                  </StyledText>
                </StyledTouchable>
              </StyledView>
            </StyledView>
          )}
        </StyledView>
      </StyledScrollView>
      <Navbar />
    </StyledView>
  );
};

export default CommandsPage;
